<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<title>Login Hotspot Politeknik Kediri</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/logopoltek.png"/>
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> -->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

<style>
* {box-sizing: border-box;}
body {font-family: Verdana, sans-serif;}
.mySlides {display: none;}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

.image-slide {
  width: 100%;
  height: 665px;
  background-size: cover;
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
</style>

</head>
<body style="background-color: #666666;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-title p-b-43">
						Login Hotspot Politeknik Kediri
					</span>
					
					<div class="wrap-input100 validate-input" data-validate = "Username mohon diisi">
						<input class="input100" type="text" name="usernm" autofocus value="">
						<span class="focus-input100"></span>
						<span class="label-input100">Username</span>
					</div>
					
					
					<div class="wrap-input100 validate-input" data-validate="Password mohon diisi">
						<input class="input100" type="password" name="passwd">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>
			

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
					
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							 <?php echo "Politeknik Kediri @" . date("Y") ?>
						</span>
					</div>

					<div class="login100-form-social flex-c-m">
						<a href="http://poltek-kediri.ac.id/" target="_blank">
							<img src="images/icons/logopoltek.png" alt="Politeknik Kediri" width="50" height="50">							
						</a>
					</div>
				</form>
				
				<!-- <div class="login100-more" style="background-image: url('images/bg-01.jpg');"> -->
				<div class="login100-more">
					<div class="slideshow-container">
						<div class="mySlides fade">
							<!-- <div class="numbertext">1 / 3</div> -->
							<!-- <img src="images/flat-simple-wallpaper-8-crop.jpg" style="height:100%; background-size: cover;"> -->
							<div class="image-slide" style="background-image: url('images/1.jpg');"></div>
							<div class="text">Caption 1</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">2 / 3</div> -->
							<div class="image-slide" style="background-image: url('images/2.jpg');"></div>
							<div class="text">Caption 2</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">3 / 3</div> -->
							<div class="image-slide" style="background-image: url('images/3.jpg');"></div>
							<div class="text">Caption 3</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">1 / 3</div> -->
							<!-- <img src="images/flat-simple-wallpaper-8-crop.jpg" style="height:100%; background-size: cover;"> -->
							<div class="image-slide" style="background-image: url('images/4.jpg');"></div>
							<div class="text">Caption 4</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">2 / 3</div> -->
							<div class="image-slide" style="background-image: url('images/5.jpg');"></div>
							<div class="text">Caption 5</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">3 / 3</div> -->
							<div class="image-slide" style="background-image: url('images/6.jpg');"></div>
							<div class="text">Caption 6</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">1 / 3</div> -->
							<!-- <img src="images/flat-simple-wallpaper-8-crop.jpg" style="height:100%; background-size: cover;"> -->
							<div class="image-slide" style="background-image: url('images/7.jpg');"></div>
							<div class="text">Caption 7</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">2 / 3</div> -->
							<div class="image-slide" style="background-image: url('images/8.jpg');"></div>
							<div class="text">Caption 8</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">3 / 3</div> -->
							<div class="image-slide" style="background-image: url('images/9.jpg');"></div>
							<div class="text">Caption 9</div>
						</div>

						<div class="mySlides fade">
							<!-- <div class="numbertext">1 / 3</div> -->
							<!-- <img src="images/flat-simple-wallpaper-8-crop.jpg" style="height:100%; background-size: cover;"> -->
							<div class="image-slide" style="background-image: url('images/10.jpg');"></div>
							<div class="text">Caption 10</div>
						</div>
					</div>
					<!-- <br> -->
					
					<!-- <div style="text-align:center">
						<span class="dot"></span> 
						<span class="dot"></span> 
						<span class="dot"></span> 
					</div> -->
				</div>
			</div>
		</div>
	</div>
	
	<script>
	var slideIndex = 0;
	showSlides();

	function showSlides() {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		//var dots = document.getElementsByClassName("dot");
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";  
		}
		slideIndex++;
		if (slideIndex > slides.length) {slideIndex = 1}    
		// for (i = 0; i < dots.length; i++) {
		// 	dots[i].className = dots[i].className.replace(" active", "");
		// }
		slides[slideIndex-1].style.display = "block";  
		//dots[slideIndex-1].className += " active";
		setTimeout(showSlides, 3000); // Change image every 2 seconds
	}
	</script>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	

</body>
</html>